package com.meiwei.app.plugin;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.os.Handler;

import com.alipay.android.app.sdk.AliPay;

public class AlipayPlugin extends CordovaPlugin {
	
	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {
		if (action.equals("payOrder")) {
			final String orderString;
			try {
				orderString = args.getString(0);
				cordova.getThreadPool().execute(new Runnable () {
	    			public void run() {
	    				AliPay alipay = new AliPay(cordova.getActivity(), mHandler);
	    				String result = alipay.pay(orderString);
	    				//callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));  
	    				callbackContext.success(result);
	    				//mHandler.sendMessage(msg);
	    			}
	    		});
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
        
        return true;  
    }  
	

	Handler mHandler = new Handler() {
		public void handleMessage(android.os.Message msg) {
			
		};
	};
}


//
//public class MeiweiAlipay extends CordovaPlugin {
//	public static final String TAG = "alipay-sdk";
//
//	private static final int RQF_PAY = 1;
//
//	private static final int RQF_LOGIN = 2;
//
//	private static final String testOrderString = "body=\"中文\"&seller_id=\"meiwei@clubmeiwei.com\"&service=\"mobile.securitypay.pay\"&_input_charset=\"UTF-8\"&it_b_pay=\"1m\"&out_trade_no=\"90020140313000002\"&payment_type=\"1\"&total_fee=\"0.01\"&sign_type=\"RSA\"&notify_url=\"http%3A//192.168.1.7%3A8000/alipay/payment_notify/\"&partner=\"2088212548769121\"&sign=\"h1YSFPXqgN6ksvfcQ6Q3k5B8gG9WA/aFAH37Sw7fRExZlP2uxM5E6Ahac7B%2B0cv2KeQm02FxGQn/zHiqDev6%2BBCfaLqmvB5X1tQKWwqTpHEe1wVqHlERSsGQtDNwI7PDFDcfZ9xSAZi5W3gSvxELE/Gtsayk3aRk4rEyCG6MP8Q%3D\"&subject=\"中文\"";
//
//	public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) throws JSONException {  
//        if (action.equals(ACTION_DATE)) {  
//            String message = args.getString(0);   
//
//    		new Thread() {
//    			public void run() {
//    				AliPay alipay = new AliPay(cordova.getActivity(), mHandler);
//    				String result = alipay.pay(orderString);
//    				Log.i(TAG, "result = " + result);
//    				Message msg = new Message();
//    				msg.what = RQF_PAY;
//    				msg.obj = result;
//    				callbackContext.sendPluginResult(new PluginResult(PluginResult.Status.OK, result));  
//                    callbackContext.success(message);   
//    				//mHandler.sendMessage(msg);
//    			}
//    		}.start();
//            
//            return true;  
//        }  
//        return false;  
//    }  
//
//	Handler mHandler = new Handler() {
//		public void handleMessage(android.os.Message msg) {
//			Toast.makeText(MeiweiAlipay.this, msg.obj.toString(), Toast.LENGTH_SHORT)
//					.show();
//		};
//	};
//}

