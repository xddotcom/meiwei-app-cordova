package com.meiwei.app.plugin;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONArray;
import org.json.JSONException;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;

import com.tencent.mm.sdk.openapi.IWXAPI;
import com.tencent.mm.sdk.openapi.SendMessageToWX;
import com.tencent.mm.sdk.openapi.WXAPIFactory;
import com.tencent.mm.sdk.openapi.WXMediaMessage;
import com.tencent.mm.sdk.openapi.WXTextObject;
import com.tencent.mm.sdk.openapi.WXWebpageObject;

@SuppressLint("SetJavaScriptEnabled")
public class WeixinPlugin extends CordovaPlugin {

	private IWXAPI api;

	public boolean execute(String action, JSONArray args,
			CallbackContext callbackContext) {
		// origin appID wx192c3e6934c60e09
		if (this.api == null) {
			api = WXAPIFactory.createWXAPI(this.cordova.getActivity(),
					"wxbc1bf354bd0919bc", false);
			api.registerApp("wxbc1bf354bd0919bc");
		}
		if (action.equals("sendAppContent")) {
			try {
				sendAppContent(action, args, callbackContext.getCallbackId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}else if(action.equals("sendTextContent")){
			try {
				sendTextContent(action, args, callbackContext.getCallbackId());
			} catch (Exception e) {
				e.printStackTrace();
			}
			return true;
		}

		return false;
	}

	private void sendTextContent(String action, final JSONArray args,
			final String callbackId) throws MalformedURLException, IOException {
		try {
			String text = args.getString(0);

			WXTextObject textObj = new WXTextObject();
			textObj.text = text;
			
			WXMediaMessage msg = new WXMediaMessage();
			msg.mediaObject = textObj;

			msg.description = text;

			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("text");
			req.message = msg;
			req.scene = SendMessageToWX.Req.WXSceneSession;
			api.sendReq(req);
		} catch (JSONException e) {
		}
	}
	
	
	private void sendAppContent(String action, final JSONArray args,
			final String callbackId) throws MalformedURLException, IOException {
		try {
			String scene = "", url = args.getString(0), text = args
					.getString(1), pic = args.getString(3);
			if (args.length() > 4) {
				scene = args.getString(4);
			}

			WXWebpageObject webpage = new WXWebpageObject();
			webpage.webpageUrl = url;

			WXMediaMessage msg = new WXMediaMessage( webpage );
			Bitmap bmp = null;
			try {
				bmp = BitmapFactory.decodeStream(new URL( pic ).openStream());
			} catch (Exception e1) {
				bmp = BitmapFactory.decodeResource( this.cordova.getActivity().getResources() , com.meiwei.app.R.drawable.nophoto_small );
			}

			Bitmap thumbBmp = Bitmap.createScaledBitmap(bmp, 150, 150, true);
			bmp.recycle();

			msg.title = text;
			msg.description = text;
			msg.thumbData = imageZoom(thumbBmp);

			SendMessageToWX.Req req = new SendMessageToWX.Req();
			req.transaction = buildTransaction("webpage");
			req.message = msg;
			try {
				req.scene = Integer.parseInt(scene);
			} catch (Exception e) {
				req.scene = SendMessageToWX.Req.WXSceneTimeline;
			}
			api.sendReq(req);
		} catch (JSONException e) {
		}
	}

	private String buildTransaction(final String type) {
		return (type == null) ? String.valueOf(System.currentTimeMillis())
				: type + System.currentTimeMillis();
	}

	private static byte[] imageZoom(Bitmap bitMap) {
 		double maxSize = 30.00;
 		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bitMap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		bitMap.recycle();
		byte[] b = baos.toByteArray();
 		double mid = b.length / 1024;
 		if (mid > maxSize) {
 			double i = mid / maxSize;
  			bitMap = zoomImage(bitMap, bitMap.getWidth() / Math.sqrt(i),
					bitMap.getHeight() / Math.sqrt(i));
		}
		try {
			baos.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return baos.toByteArray();
	}

	/***
	 * 图片的缩放方法
	 */
	public static Bitmap zoomImage(Bitmap bgimage, double newWidth,
			double newHeight) {
 		float width = bgimage.getWidth();
		float height = bgimage.getHeight();
 		Matrix matrix = new Matrix();
 		float scaleWidth = ((float) newWidth) / width;
		float scaleHeight = ((float) newHeight) / height;
 		matrix.postScale(scaleWidth, scaleHeight);
		Bitmap bitmap = Bitmap.createBitmap(bgimage, 0, 0, (int) width,
				(int) height, matrix, true);
		return bitmap;
	}

}
